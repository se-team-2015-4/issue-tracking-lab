unit ChangeMatrix;
interface
const
    DimensionRows = 7;
    DimensionColumns = 7;
    MinMatrixRangeValue = -5;
    MaxMatrixRangeValue = 7;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;
procedure ReplaceMinimalElementOnArithmeticMean(var matrix: Matrix; numberRowMinimalElement, numberColumnMinimalElement: Integer; arithmeticMean: Real);

implementation
procedure ReplaceMinimalElementOnArithmeticMean(var matrix: Matrix; numberRowMinimalElement, numberColumnMinimalElement: Integer; arithmeticMean: Real);
begin
    matrix[numberRowMinimalElement, numberColumnMinimalElement] := arithmeticMean;
end;

end.

