program issue_tracking;
uses CreateMatrix, OperationsWithMatrix, ChangeMatrix;
const
    DimensionRows = 7;
    DimensionColumns = 7;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;
var
    matrix1: Matrix;
    mini: Integer;
begin
    CreateMatrix.FillMatrix(matrix1);
    CreateMatrix.WriteMatrix(matrix1);
    OperationsWithMatrix.GetMinimalElementAndItsPositionOnMainDiagonal(matrix1, mini);
    WriteLn('Arithmetic mean: ', OperationsWithMatrix.GetArithmeticMeanElementsBelowAntiDiagonal(matrix1):0:2);
    ChangeMatrix.ReplaceMinimalElementOnArithmeticMean(matrix1, mini, mini, OperationsWithMatrix.GetArithmeticMeanElementsBelowAntiDiagonal(matrix1));
    CreateMatrix.WriteMatrix(matrix1);
    ReadLn;
end.

