unit OperationsWithMatrix;
interface
const
    DimensionRows = 7;
    DimensionColumns = 7;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;

procedure GetMinimalElementAndItsPositionOnMainDiagonal(var matrix: Matrix; mini: Integer);
function GetArithmeticMeanElementsBelowAntiDiagonal(var matrix: Matrix): Real;

implementation
procedure GetMinimalElementAndItsPositionOnMainDiagonal(var matrix: Matrix; mini: Integer);
var
    i: Integer;
begin
    mini := 1;
    for i := 1 to DimensionRows do
        begin
            if matrix[mini, mini] > matrix[i, i] then
            begin
                mini := i;
            end;
        end;
    WriteLn('Minimal element:', matrix[mini, mini]:0:2);
    WriteLn('Position of minimal element:');
    WriteLn('i: ', mini, ' j: ', mini);
end;
function GetArithmeticMeanElementsBelowAntiDiagonal(var matrix: Matrix): Real;
var
    i, j: Integer;
begin
    GetArithmeticMeanElementsBelowAntiDiagonal := 0;
    for i:=1 to DimensionRows do
    begin
        for j := DimensionColumns + 2 - i to DimensionColumns do
        begin
            begin
                GetArithmeticMeanElementsBelowAntiDiagonal := GetArithmeticMeanElementsBelowAntiDiagonal + matrix[i,j];
            end
        end;
    end;
    GetArithmeticMeanElementsBelowAntiDiagonal := GetArithmeticMeanElementsBelowAntiDiagonal / 21;
end;
end.

